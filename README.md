# TP TERRAFORM - 5SRC

## Objectif:

Utiliser Terraform pour déployer et configurer automatiquement un site WordPress sur une instance EC2, avec le contenu du dossier /wp-content placé dans un bucket S3.

### Etapes:

1. Installer Terraform sur votre ordinateur
2. Créer un compte AWS (ou bien utiliser votre compte AWS Academy) et générer les clés d'accès
3. Créer un fichier Terraform pour définir les ressources AWS à utiliser (instance EC2, bucket S3)
4. Définir les variables Terraform pour les paramètres de configuration (nom du bucket S3, etc.)
5. Ecrire le code Terraform pour créer et configurer l'instance EC2 (installation de WordPress, etc.)
6. Ecrire le code Terraform pour placer le contenu du dossier /wp-content dans le bucket S3
7. Tester le déploiement en utilisant la commande terraform apply
8. Modifier le code Terraform pour mettre à jour le site WordPress
9. Tester les mises à jour en utilisant la commande terraform apply
10. Supprimer les ressources AWS en utilisant la commande terraform destroy

Notes:

Vous pouvez utiliser un modèle prédéfini pour WordPress disponible sur le site Terraform Registry (https://registry.terraform.io/)
N'oubliez pas de sécuriser vos clés d'accès AWS et de les stocker dans un endroit sûr
Assurez-vous de bien comprendre comment Terraform fonctionne avant de commencer ce TP (lecture de la documentation, tutoriels en ligne, etc.)


NB: Si vous utilisez une compte AWS academy, vous ne pourrais pas utilisé le service de bases de donnée managée 
